<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks`.
 * Has foreign keys to the tables:
 *
 * - `todolists`
 */
class m180730_064607_create_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tasks', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'todolist_id' => $this->integer()->unsigned()->notNull(),
            'done' => $this->boolean()->defaultValue(false),
        ]);

        // creates index for column `todolist_id`
        $this->createIndex(
            'idx-tasks-todolist_id',
            'tasks',
            'todolist_id'
        );

        // add foreign key for table `todolists`
        $this->addForeignKey(
            'fk-tasks-todolist_id',
            'tasks',
            'todolist_id',
            'todolists',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `todolists`
        $this->dropForeignKey(
            'fk-tasks-todolist_id',
            'tasks'
        );

        // drops index for column `todolist_id`
        $this->dropIndex(
            'idx-tasks-todolist_id',
            'tasks'
        );

        $this->dropTable('tasks');
    }
}
