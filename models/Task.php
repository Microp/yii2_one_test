<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 27/07/2018
 * Time: 15:00
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 */
class Task extends ActiveRecord
{

    public static function tableName()
    {
        return 'tasks';
    }

}