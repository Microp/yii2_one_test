<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 27/07/2018
 * Time: 15:00
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * @property string name
 * @property int user_id
 */
class Todolist extends ActiveRecord
{
//    public $id;
//    public $name;
//    public $max_tasks;
//    public $user_id;


    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'todolists';
    }

    public function rules()
    {
        return [
            [['name', 'user_id'], 'required']
        ];
    }


    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['todolist_id' => 'id']);
    }

}