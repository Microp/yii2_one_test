<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 31/07/2018
 * Time: 10:09
 */

namespace app\modules\api\controllers;


use app\models\Task;
use app\models\Todolist;
use yii\rest\ActiveController;
use yii\web\Response;

class TaskController extends DefaultActiveController
{
    public $modelClass = 'app\models\Task';

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
//            'create' => '$this->actionCreate()',
//            [
//                'class' => 'yii\rest\CreateAction',
//                'modelClass' => $this->modelClass,
//                'checkAccess' => [$this, 'checkAccess'],
//                'scenario' => $this->createScenario,
//            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->updateScenario,
            ],
            'delete' => [
                'class' => 'yii\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }


    public function actionChange_state()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $request = \Yii::$app->request;
        $id = $request->getBodyParam('id');

        $task = Task::findOne($id);
        if (!is_null($task)) {
            \Yii::$app->response->statusCode = 200;
            $task->done = ($task->done ? false : true);
            $task->save();
            return '';
        }

        throw new \yii\web\NotFoundHttpException();
    }

    public function actionCreate()
    {

//        return ['sd'];
        $data = \Yii::$app->request->getBodyParams();
        $todolist = Todolist::findOne($data['todolist_id']);


        if (\Yii::$app->user->can('createTask', ['todolist' => $todolist])) {


            $task = new Task();
            $task->name = $data['name'];
            $task->todolist_id = $todolist->id;
            $task->save();

            \Yii::$app->response->statusCode = 201;
            return $task;


//            parent::createAction(\Yii::$app->request);

        }


        \Yii::$app->response->statusCode = 403;
        return ['error' => ['message' => "You cannot create more than $todolist->max_tasks tasks"]];
    }


    public function actionBy_todolist($todolist_id)
    {
        $todolist = Todolist::findOne($todolist_id);
        return [
            'todolist' => $todolist,
            'tasks' => $todolist->tasks,
        ];
    }

}