<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 27/07/2018
 * Time: 11:01
 */

namespace app\models;

use Yii;
use yii\base\Model;

class EntryForm extends Model
{
    public $name;
    public $email;

    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            ['email', 'email'],
        ];
    }

}