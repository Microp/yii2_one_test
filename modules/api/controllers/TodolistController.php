<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 30/07/2018
 * Time: 17:42
 */

namespace app\modules\api\controllers;

use app\models\Todolist;
use yii\rest\ActiveController;

class TodolistController extends DefaultActiveController
{
    public $modelClass = 'app\models\Todolist';

//    public function actionIndex()
//    {
//        return Todolist::find()->all();
//    }

    public function actionBy_user()
    {
        $user_id = \Yii::$app->user->id;
        return Todolist::find()->where(['=', 'user_id', "$user_id"])->all();
    }



}