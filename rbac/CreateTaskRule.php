<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 31/07/2018
 * Time: 16:07
 */

namespace app\rbac;


use app\models\Task;
use yii\rbac\Item;
use yii\rbac\Rule;

class CreateTaskRule extends Rule
{
    public $name = 'createTask';


    /**
     * Executes the rule.
     *
     * @param string|int $user the user ID. This should be either an integer or a string representing
     * the unique identifier of a user. See [[\yii\web\User::id]].
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to [[CheckAccessInterface::checkAccess()]].
     * @return bool a value indicating whether the rule permits the auth item it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (!isset($params['todolist'])) {
            return false;
        }
        $todolist = $params['todolist'];

//        echo ($todolist->max_tasks > Task::find()->where(['=', 'todolist_id', "$todolist->id"])->count()) ? true : false;
//        exit();

        return ($todolist->max_tasks > Task::find()->where(['=', 'todolist_id', "$todolist->id"])->count()) ? true : false;
    }
}