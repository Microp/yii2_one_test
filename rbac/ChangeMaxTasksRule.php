<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 01/08/2018
 * Time: 12:34
 */

namespace app\rbac;


use app\models\Task;
use yii\rbac\Item;
use yii\rbac\Rule;

class ChangeMaxTasksRule extends Rule
{
    public $name = 'changeMaxTasks';

    /**
     * Executes the rule.
     *
     * @param string|int $user the user ID. This should be either an integer or a string representing
     * the unique identifier of a user. See [[\yii\web\User::id]].
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to [[CheckAccessInterface::checkAccess()]].
     * @return bool a value indicating whether the rule permits the auth item it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (!isset($params['todolist']) && !isset($params['value'])) {
            return false;
        }

        $todolist = $params['todolist'];
        $value = $params['value'];

//        echo Task::find()->where(['=', 'todolist_id', "$todolist->id"])->count();
//        echo $value;
//        exit();

        if (Task::find()->where(['=', 'todolist_id', "$todolist->id"])->count() > $value) {
            \Yii::$app->getSession()->setFlash('flashMessage', 'You cannot reduce count of max tasks lower than it is now');
            return false;
        }

        return true;
    }
}