run next commands

php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations

yii migrate --migrationPath=@vendor/filsh/yii2-oauth2-server/migrations

yii migrate --migrationPath=@yii/rbac/migrations

php yii migrate

php yii seed
php yii rbac/init

now you can use 2 users:

    admin:
        username: admin
        email: admin@test.com
        password: secret
        roles: admin, superAdmin

    second:
        username: second
        email: second@test.com
        password: secret
        roles: admin
