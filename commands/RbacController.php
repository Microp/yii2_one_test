<?php /** @noinspection PhpUnhandledExceptionInspection */

/**
 * Created by PhpStorm.
 * User: avn
 * Date: 31/07/2018
 * Time: 14:18
 */

namespace app\commands;


use app\models\User;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    /*
        * User:
        *  createTodolist
        *  createTask
        *
        * Admin:
        *  showUsers
        *
        * SuperAdmin:
        *  changeMaxTasks
    *
    */

    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        // добавляем разрешение "createTodolist"
        $createTodolist = $auth->createPermission('createTodolist');
        $createTodolist->description = 'Create a todolist';
        $auth->add($createTodolist);


        // добавляем разрешение "showTodo"
        $showTodo = $auth->createPermission('showTodoTodo');
        $showTodo->description = 'show users todolists and tasks';
        $auth->add($showTodo);


        // add the rule
        $rule = new \app\rbac\CreateTaskRule;
        $auth->add($rule);


        // добавляем разрешение "createTask"
        $createTask = $auth->createPermission('createTask');
        $createTask->description = 'Create a task';
        $createTask->ruleName = $rule->name;
        $auth->add($createTask);

        // add the rule change max tasks
        $rule = new \app\rbac\ChangeMaxTasksRule();
        $auth->add($rule);


        // добавляем разрешение "changeMaxTasks"
        $changeMaxTasks = $auth->createPermission('changeMaxTasks');
        $changeMaxTasks->description = 'change max count of todolists tasks';
        $changeMaxTasks->ruleName = $rule->name;
        $auth->add($changeMaxTasks);


        // добавляем роль "role_user" и даём роли разрешение "createTodolist", "createTask"
        $role_user = $auth->createRole('role_user');
        $auth->add($role_user);
        $auth->addChild($role_user, $createTodolist);
        $auth->addChild($role_user, $createTask);

        // добавляем роль "role_admin" и даём роли разрешение "showTodo"
        $role_admin = $auth->createRole('role_admin');
        $auth->add($role_admin);
        $auth->addChild($role_admin, $showTodo);


        // добавляем роль "role_superAdmin" и даём роли разрешение "showTodo"
        $role_superAdmin = $auth->createRole('role_superAdmin');
        $auth->add($role_superAdmin);
        $auth->addChild($role_superAdmin, $changeMaxTasks);
        $auth->addChild($role_superAdmin, $role_admin);


        $user_admin = User::findByUsername('admin');
        $user_second = User::findByUsername('second');

        // Назначение ролей пользователям. 1 и 2 это IDs возвращаемые IdentityInterface::getId()
        // обычно реализуемый в модели User.
        $auth->assign($role_superAdmin, $user_admin->id);
        $auth->assign($role_user, $user_admin->id);
        $auth->assign($role_admin, $user_second->id);
    }

    public function actionReset()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }

}