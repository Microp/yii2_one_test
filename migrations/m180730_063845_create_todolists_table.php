<?php

use yii\db\Migration;

/**
 * Handles the creation of table `todolists`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m180730_063845_create_todolists_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('todolists', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'max_tasks' => $this->integer()->defaultValue(5),
            'user_id' => $this->integer()->unsigned()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-todolists-user_id',
            'todolists',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-todolists-user_id',
            'todolists',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-todolists-user_id',
            'todolists'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-todolists-user_id',
            'todolists'
        );

        $this->dropTable('todolists');
    }
}
