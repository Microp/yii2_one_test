<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 27/07/2018
 * Time: 11:18
 */

use yii\helpers\Html;

?>

<p>You have entered the following information:</p>

<ul>
    <li><label>Name</label>: <?= Html::encode($model->name) ?></li>
    <li><label>Email</label>: <?= Html::encode($model->email) ?></li>
</ul>